const express = require('express');
const router = express.Router();
const fs = require('fs');

const taskSchema = require('../models/tasks');

//************************************************************************************************
//render Index
const getiIndex = (req, res)=> {
    taskSchema.find({'status': false},(err,taskList)=>{
        res.render('index',{
            title:'Tasks',
            taskList: taskList
        });
    })

};

//************************************************************************************************
//functions for CRUD
//get all tasks
const getTasks = (req,res)=>{
    taskSchema.find()
        .then(tasks => {
            res.json(tasks);
        })
        .catch(err => {
            res.send(err);
        });

};
//************************************************************************************************
//get Task by status
const getTaskbyStatus = (req, res)=>{
    taskSchema.find({'status': false},(err, task)=>{
        if(err){
            console.log(err);
        }else{
            res.json(task);
        }
    })
};


//get Task by id
const getTask = (req, res)=>{
    taskSchema.findById(req.params._id)
        .then(task =>{
            res.json(task)
        })
        .catch(err =>{
            res.send(err);
        })
};
//************************************************************************************************
//save Task to DB
const addTask = (req, res)=>{
    const newTask = new taskSchema(req.body);
    newTask.save()
        .then(task=>{
            res.json(task);
            //console.log(task)
            console.log('Task added!!!!!')
           // res.send('/');
        })
        .catch(err=>{
            res.status(400);
            res.send(err);
        })

};
//************************************************************************************************
//updateTask startDate
const updateStartDate = (req, res)=> {
    taskSchema.findById(req.params._id)
        .then(task => {
            Object.assign(task, task.startDate = Date.now()).save()
                .then(task => {
                    res.json(task);
                })
                .catch(err => {
                    res.status(400);
                    res.send(err);
                })
        })
        .catch(err => {
            res.status(400);
            res.send(err);
        })
};

//updateTask endDate
const updateEndDate = (req, res)=> {
    taskSchema.findById(req.params._id)
        .then(task => {
            Object.assign(task, task.endDate = Date.now()).save()
                .then(task => {
                    res.json(task);
                })
                .catch(err => {
                    res.status(400);
                    res.send(err);
                })
        })
        .catch(err => {
            res.status(400);
            res.send(err);
        })
};
//updateTask status
const updateStatus = (req, res)=>{
    taskSchema.findById(req.params._id)
        .then(task =>{
            Object.assign(task, task.status = true).save()
                .then(task =>{
                    res.json(task);
                })
                .catch(err =>{
                    res.status(400);
                    res.send(err);
                })
        })
        .catch(err =>{
            res.status(400);
            res.send(err);
        })
};

//updateTask
const updateTask = (req, res)=>{
    taskSchema.findById(req.params._id)
        .then(task =>{
            Object.assign(task, req.body).save()
                .then(task =>{
                    res.json(task);
                })
                .catch(err =>{
                    res.status(400);
                    res.send(err);
                })
        })
        .catch(err =>{
            res.status(400);
            res.send(err);
        })
};
//************************************************************************************************
//delete Task from DB
const deleteTask = (req, res)=>{
    taskSchema.findByIdAndRemove(req.params._id)
        .then(()=>{
            res.status(204);
            res.send('');
        })
        .catch(err=> res.send(err));
};


//************************************************************************************************
//hier write csv Datastream
const createCSV = (req, res)=>{
    var filename = "tasksFile.csv";

    taskSchema.find().lean().exec({},(err, tasks)=>{
        if (err) {
            res.send(err);
        }else{
            var data = JSON.stringify(tasks, null, 2);
            res.status(200);
            res.send('Downloaded File');

            fs.writeFile(filename,data,()=>{
                console.log('downloaded csv')
            });
        }
    })
};


//************************************************************************************************
//neue route für csv dazu nehmen
router.route('/export')
    .get(createCSV)
//index route
router.route('/')
    .get(getiIndex)
//get and add tasks route
router.route('/tasks')
    .get(getTasks)
    .post(addTask)
//get one and delete and update route
router.route('/task/:_id')
    .get(getTask)
    .delete(deleteTask)
    .patch(updateTask)
router.route('/tasksbystatus')
    .get(getTaskbyStatus)

//für testing gibt es die näachsten routes
router.route('/updatestartDate/:_id')
    .patch(updateStartDate)
router.route('/updateendDate/:_id')
    .patch(updateEndDate)
router.route('/updatestatus/:_id')
    .patch(updateStatus)

module.exports = router;