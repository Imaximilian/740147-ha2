import { Subscription, Observable } from 'rxjs';
import { DataServiceService } from './../data-service.service';
import { Task } from './../task';
import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  ticks = 0;
  private timer;
  private sub: Subscription;

  @Input()
  task: Task;

  @Output()
  delete: EventEmitter<Task> = new EventEmitter();

  @Output()
  status: EventEmitter<Task> = new EventEmitter();

  @Output()
  startDate: EventEmitter<Task> = new EventEmitter();

  @Output()
  endDate: EventEmitter<Task> = new EventEmitter();

  @Output()
  duration: EventEmitter<Task> = new EventEmitter();

  @Output()
  updateTask: EventEmitter<Task> = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  toggleStatus(task: Task) {
    this.status.emit(task);
  }

  deleteTask(task: Task) {
    this.delete.emit(task);
  }

  updateDuration(task: Task) {
    console.log('timer stop!');
    console.log(task);
    this.sub.unsubscribe();
    this.duration.emit(task);
  }

  updateStartDate(task: Task) {
    this.timer = Observable.timer(1, 1000);
    this.sub = this.timer.subscribe(t => this.ticker(t));
    this.startDate.emit(task);
  }

  ticker(tick){
    console.log(this);
    this.ticks = tick;
  }

  updateEndDate(task: Task){
    this.endDate.emit(task);
  }
  updatefullTask(task: Task){
    this.updateTask.emit(task);
  }

}
