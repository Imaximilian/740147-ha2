import { Task } from './../task';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  @Input()
  taskList: Task[];

  @Output()
  remove: EventEmitter<Task> = new EventEmitter();

  @Output()
  toggleStatus: EventEmitter<Task> = new EventEmitter();

  @Output()
  startDate: EventEmitter<Task> = new EventEmitter();

  @Output()
  endDate: EventEmitter<Task> = new EventEmitter();

  @Output()
  duration: EventEmitter<Task> = new EventEmitter();

  @Output()
  updateTask: EventEmitter<Task> = new EventEmitter();



  constructor() { }

  ngOnInit() {
  }

  onRemoveTask(task: Task){
    this.remove.emit(task);
  }

  ontoggleStatus(task: Task) {
    this.toggleStatus.emit(task);
  }

  onStartDate(task: Task) {
    this.startDate.emit(task);
  }

  onEndDate(task: Task) {
    this.endDate.emit(task);
    this.duration.emit(task);
  }

  onupdatefullTask(task: Task) {
    this.updateTask.emit(task);
  }

  updateDuration(task: Task) {
    this.duration.emit(task);
  }


}
