import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import {DataServiceService} from './data-service.service';
import { TaskcreateComponent } from './taskcreate/taskcreate.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskComponent } from './task/task.component';
import { FooterComponent } from './footer/footer.component';
import { TimerComponent } from './timer/timer.component';



@NgModule({
  declarations: [
    AppComponent,
    TaskcreateComponent,
    TaskListComponent,
    TaskComponent,
    FooterComponent,
    TimerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule    
  ],
  providers: [DataServiceService],
  bootstrap: [AppComponent]//sagt an was als erstes angezeit wird
})
export class AppModule { }
