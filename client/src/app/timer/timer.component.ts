import { Observable, Subscription } from 'rxjs';
import { DataServiceService } from './../data-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

  ticks = 0;

  minutes: number = 0;
  hours: number = 0;
  days: number = 0;

  sub: Subscription;

  constructor(private dataServeice: DataServiceService) { 
    this.startTimer();
  }

  ngOnInit() {
   
  }

  private startTimer(){
    let timer = Observable.timer(2000,1000);
    this.sub = timer.subscribe(t =>{
      this.ticks  =t;
      this.minutes = this.getMinutes(this.ticks);
      this.hours = this.getHours(this.ticks);
      this.days = this.getDays(this.ticks);

    });
  }

  private getMinutes(ticks: number){
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number){
    return this.pad((Math.floor(ticks / 60)) / 60);
  }
  private getDays(ticks: number){
    return this.pad((Math.floor(ticks / 60)) % 24);
  }

  private pad(digit: any){
    return digit <= 9 ? '0' + digit : digit;
  }

}
