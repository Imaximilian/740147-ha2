import { TimerComponent } from './timer/timer.component';
import { Injectable } from '@angular/core';
import {Task} from './task';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';


@Injectable()
export class DataServiceService {

  taskList: Task[] = [];

  constructor(private http: Http) {
    console.log(" service running...")
   }




// *********************************************************
// get all /tasks
getAllTask(): Observable<Task[]>{
  return this.http.get('http://localhost:3000/tasks')
    .map(res => res.json())
    .catch(this.handleErrorObservable);
}

// post /tasks
addTask(newtask: Task): Observable<Task> {
  console.log(newtask);
  let headers = new Headers({'Content-Type':'application/json'});
  return this.http.post('http://localhost:3000/tasks', JSON.stringify(newtask),{headers: headers})
    .map(res => res.json())
    .catch(this.handleErrorObservable);
}




// update /task/:id
upDateTaskStartDateById(task): Observable<Task>{
    return this.http.patch('http://localhost:3000/updatestartDate/' + task._id, JSON.stringify(task=
    {startDate: Date.now()}))
    .map(res =>res.json())
    .catch(this.handleErrorObservable);
}

upDateTaskEndDateById(task): Observable<Task>{
  return this.http.patch('http://localhost:3000/updateendDate/' + task._id, JSON.stringify(task=
    {endDate: Date.now()}))
    .map(res => res.json())
    .catch(this.handleErrorObservable);
}
updateTaskById(task): Observable<Task>{
  console.log(task);
  return this.http.patch('http://localhost:3000/task/' + task._id, task)
    .map(res => {new Task (res.json());
    })
    .catch(this.handleErrorObservable);
}

toggleStatus(task): Observable<Task>{
  return this.http.patch('http://localhost:3000/updatestatus/' + task._id, 
    task)
    .map(res => {new Task (res.json())})
    .catch(this.handleErrorObservable);

}






// delete /task/:id
deleteTask(task): Observable<null>{
  return this.http.delete('http://localhost:3000/task/' + task._id)
  .map(res => null)
  .catch(this.handleErrorObservable);
}


getTaskbyId(task){
  return this.http.get('http://localhost:3000/task/'+ task._id)
    .map(res=>{
      return new Task(res.json());
    })
  /*return this.taskList
  .filter(t => task.id === t.id)
  .pop();*/
}




private handleErrorObservable(error: Response | any){
    console.error(error.message || error);
    return Observable.throw(error.message || error);
}

}


