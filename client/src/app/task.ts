export class Task {

    id: number;
    title: string = '';
    status: boolean = false;
    startDate: Date;
    endDate: Date;
    createDate: Date;
    description: string = '';
    duration: number;

    constructor(task: Object={}){
        Object.assign(this,task);
    }

}
