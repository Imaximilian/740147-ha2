import { Task } from './../task';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-taskcreate',
  templateUrl: './taskcreate.component.html',
  styleUrls: ['./taskcreate.component.css']
})
export class TaskcreateComponent implements OnInit {

  newtask: Task = new Task();

  @Output()
  add: EventEmitter<Task> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
 
  addTask(){
    this.add.emit(this.newtask);
    this.newtask = new Task();
  }

}
