import { Component, OnInit } from '@angular/core';
import { DataServiceService } from './data-service.service';
import { Task } from './task';
import {Angular2Csv} from 'angular2-csv/Angular2-csv'


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataServiceService]
})
export class AppComponent {

  title: 'Task-App';
  taskList: Array<any>;
  task: Task = new Task();

  constructor(private dataService: DataServiceService) {  }

  ngOnInit(){
    this.dataService.getAllTask()
      .subscribe(tasks => {
        console.log(tasks);
        this.taskList = tasks;
      });
  }

  onAddTask(task: Task) {
    this.dataService.addTask(task)
      .subscribe((task) => {
        this.taskList.push(task);
      });
  }


  onDeleteTask(task) {
    this.dataService.deleteTask(task)
    .subscribe((res) => {
      this.taskList = this.taskList
        .filter((t) => t.id !== task.id);
    });
  }




  onupdateStartDate(task) {
    this.dataService.upDateTaskStartDateById(task)
      .subscribe(task => {
      });
  }
  onupdateEndDate(task) {
    this.dataService.upDateTaskEndDateById(task)
      .subscribe(task => {
      });
  }

  onupdateTask(task){
    this.dataService.updateTaskById(task)
    .subscribe(newtask => {
      task = newtask;
    });
  }

  onStatusComplete(task) {
    this.dataService.toggleStatus(task)
      .subscribe(t => {
        t = task;

      });
  }
// ******************************************************  */
  onexportCsv(){
    this.dataService.getAllTask()
      .subscribe(data => {
        this.taskList = data;

        new Angular2Csv(this.taskList,'MyCsv');
      })
  }

}
