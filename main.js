//import modules
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const ip = require('ip');
const cors = require('cors');

const app  =  express();
const port = 3000;

const tasks = require('./routes/tasks');

//beide funktionen erlauben CORS 
app.use(cors());
/*app.use(function (req, res, next) {  // middleware logs routes to broker
    
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
  })
*/

/*werden nicht mehr benötigt
set view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.engine('html', require('pug').renderFile);
*/

//body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', tasks);



//listen port 3000
app.listen(port, function() {
    console.log('Server ip:  ' + ip.address() + ' listen to: '+ port)
    console.log("Cors enabled for all origins");
})

//database mongoose
mongoose.connect('mongodb://localhost:27017/mydb');
let db = mongoose.connection;
//on connection & check for db errors
mongoose.connection.on('connected',()=>{
    console.log('connected to mongo DB @27017!')
})

mongoose.connection.on('error',(err)=>{
   if(err){
     console.log('Error DB connection' + err)
   }
})