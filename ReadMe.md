# FWE HA1

#### Time-Tracking-App

Eine App die diverse Aufgaben zeitlich tracken kann. Man kann mehrere Aufgaben hinzufügen und löschen, außerdem kann
man die Zeit starten und stoppen und den Status der Aufgabe ändern.


## Tech/Framework used

- Nodejs
- Express 
- body-parser 
- MongoDB
- mongoose
- pug
- nodemon
- ip
- path
- fs


## Features

Die App verfügt über eine simple Oberfläche mit der man Aufgaben + Beschreibung hinzufügen kann.
Diese werden dann auf der Seite angezeigt(manchmal muss man diese auch neu laden).
Beim erstellen einer Aufgabe wird ein "createDate"(erstell Datum) mit gesetzt.
Um die Aufgabe zu starten bzw. zu beenden gibt man die update URL in Postman ein und schreibt dort ein Datum rein.
Außerdem kann man alle Einträge, aus der Datenbank, in eine CSV-Datei exportieren lassen.


## Installation

Zu Installierende Software:

NodeJS (enthält node package manager -> npm)
Der Ordner node_modules, welcher alle npm packages enthält, welcher verwendet wird, wird nicht auf Gitlab versioniert. Daher ein Eintrag node_modules im File .gitignore. Um die packages zu installieren, einfach in den Projekt-Ordner navigieren und dort npm install ausführen:

+ npm install

Nach der Installation der node modules kann der Server gestartet werden.Es sollte auch das package "nodemon" installiert sein welcehs dafür sorgt das man
nicht immer den Server manuell neu starten muss. Um den dann zu starten gibt man das folgende ein:

+ npm run dev


Der Vorgang startet nur den Server, jetzt fehlt noch die Datenbank.

Also muss noch MongoDB installiert werden, unter dieser URL :
https://www.mongodb.com/download-center#community
Hat man das installiert begibt man sich in den MongoDB ordner und startet dort die "mongod.exe" und die "mongo.exe". 

So nun ist auch die Datenbank fertig und man kann die App testen, dazu im nächsten kapitel mehr.



## Tests
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/8f9c066eb98470e01445)

In der Postman Collection sind 5 URLs vorhanden.
+ get     : zeigt alle Aufgaben an
+ post    : fügt eine Aufgabe hinzu
+ patch   : updated eine Aufgabe mit dem was man im Body eingiebt
+ delete  : löscht eine Aufgabe
+ get     : exportiert die Datenbank in eine CSV Datei(die Datei befindet sich im selben Verzeichnis wo die main.js liegt)

