var mongoose = require('mongoose');

//Schema for tasks
const TaskSchema = mongoose.Schema({
    //id automatic generated
    title: {
        type: String,
        required: true
    },
    status:{
        type: Boolean,
        default: false
    },
    startDate: {
        type: Date

    },
    endDate:{
        type: Date

    },
    createDate:{
        type: Date,
        default: Date.now
    },
    description:{
        type: String
    },
    duration:{
        type: Date
    }

});

let schema = module.exports = mongoose.model('Task', TaskSchema);

